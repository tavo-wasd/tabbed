# My personal tabbed build

## Description
My personal repository for a custom tabbed build currently using the Gruvbox theme.

## Dependencies (Archlinux names)
make,
libxcb
libX11-devel,
libXft-devel,
libXinerama-devel,
